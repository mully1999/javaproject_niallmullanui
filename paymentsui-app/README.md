Very frustrating...

I spend most of the week reading around Redux and can't actually say I understand it any better.
I wanted to implement several features but could not with redux, I have left my code in so you can see what I was trying to achieve even though I could not get it working in most cases.

Here is a list of things I could not get working~
1. AddPayment.js I wanted to just dispatch the addPayment action but
could not get it working, I ended up just coding to a function instead in AssPaymentFn. I want to see both ways of doing it though no idea how to make connect, 
mapDispatchToProps & mapStateToProps despite following loads of online guides and watching numerous videos
2. In the AddPaymentFn form I wanted to use a calculated value for the payment id rather than have the user enter one. It makes no sense to enter one as the user will not be aware of the next highest id. If repeat an id then they simply overwrite an existing payment instead of adding a new one.
I have the function to calculate the next id from the selector of the payments state but could not get it to update the payment object no matter what I did.
3. I want to prepopulate the fields in the Edit Page with the Payment I return, but the useSelector value does not appear to become available until after I try and populate the values in the the initial state? what is the lifecycle of these functions, how do I know when stuff will be available to use????
4. I wanted to filter the actual redux store of pymentsList rather than making a fresh call to mongo in the list page i..e treat the initPaymentList call like a cache that could be filtered for display 
 rather than calling mongo every time 

Things I did manage to get done were ~
1. Add Payment to mongo and redux store in one go
2. Store payment list in redux
3. Setup the about page
4. Routing
5. Used a slicer and react redux template to create project
6. Dispatched from the App.js to initalise the store, rather than having to make a user click a button to get ht einitial data
  
I defintely feel like I do not understand redux at this stage.
I'm stopping the project today (Thr1/10/20) as I need to get other stuff done before the weekend.
  
