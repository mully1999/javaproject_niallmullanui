import { configureStore } from '@reduxjs/toolkit';
import paymentsReducer from '../features/payments/paymentsSlice';

export default configureStore({
  reducer: {
    payments: paymentsReducer
  },
});
