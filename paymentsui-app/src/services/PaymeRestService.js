import http from "../http-common.js"

export class PayMeService {
    getAll() {
        return http.get("/find");
    }

    count() {
        return http.get("/count");
    }

    get(id){
        return http.get(`/find/id/${id}`)
    }

    getByType(type){
        return http.get(`/find/type/${type}`)
    }
    create(data){
        return http.post("/save", data)
    }

    status(){
        return http.get("/status")
    }
}

export default new PayMeService();