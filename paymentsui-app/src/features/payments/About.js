import React, {Component} from 'react'



class About extends Component{
    constructor(props){
        super();
        this.state = {
            name:"Welcome to the React UI Payments App",
            apiStatus:"Unknown"}
    }

    render(){
        return (
            <div class={"jumbotron"}>
                <h1>{this.state.name}<br /><br /></h1>
                <h2>Current status: <br /><h3>{this.state.apiStatus}</h3></h2>
            </div>
        )
    }

    // TODO Update using RestService class
    //doing it like this as I cannot work out how to dispatch actions from a Class despite reading all week!
    async componentDidMount(){
        let url = 'http://localhost:8080/payme/status';
        let textData
        try{
            let response = await fetch('http://localhost:8080/payme/status');
            textData = await response.text();
            console.log('The response is: ' + textData)
        }
        catch (e){
            console.log('The err is: ' +e.toString());
        }
        this.setState({apiStatus:textData});
    }
}

export default About;