import React, {Component} from 'react';
import {Redirect} from "react-router-dom";
import PayMeService from "../../services/PaymeRestService";
import { connect } from 'react-redux'
import {addPayment} from "./paymentsSlice";

class AddPayment extends Component {
    constructor(props) {
        super(props);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangeCustId = this.onChangeCustId.bind(this);
        this.savePayment = this.savePayment.bind(this);

        this.state =   {
            //This needs to be set off the latest value in the list once we get it into redux
            "id": 4,
            "paymentDate": new Date(),
            "type": '',
            "amount": '',
            "custId": ''
        };
    }

    savePayment() {
        var data = {
            id: this.state.id,
            paymentDate: this.state.paymentDate,
            type: this.state.type,
            amount: this.state.amount,
            custId: this.state.custId
        };
        console.log(data);
        //TODO ***CANNOT GET THIS TO WORK
        //Update Redux store
        //this.props.addPayment(data)
        //Update MongoStore
        PayMeService.create(data)
            .then(response => {
                this.setState({
                    submitted: true
                }
                );

            })
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    onChangeType(e) {
        this.setState({
            type: e.target.value
        });
    }

    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }

    onChangeCustId(e) {
        this.setState({
            custId: e.target.value
        });
    }

    render() {

        if (this.state.submitted){
            return <Redirect to={"home"} />
        }
        return (
            <form>
                <h1>Add Payment</h1>
                <div className="form-group">
                    <label htmlFor="paymentType">Type</label>
                    <input type="text" className="form-control" id="paymentType" aria-describedby="typeHelp"
                           placeholder="Enter Type" value={this.state.type} onChange={this.onChangeType}/>
                    <small id="paymentTypeHelp" className="form-text text-muted">Valid payment types are CASH, CHEQUE or CC</small>
                </div>
                <div className="form-group">
                    <label htmlFor="paymentamount">Amount</label>
                    <input type="number" className="form-control" id="paymentAmount" aria-describedby="amountHelp"
                           placeholder="Enter Amount" value={this.state.amount} onChange={this.onChangeAmount}/>
                    <small id="amountHelp" className="form-text text-muted">Please enter a number greater than 0</small>
                </div>
                <div className="form-group">
                    <label htmlFor="custId">Customer Id</label>
                    <input type="custId" className="form-control" id="custId" aria-describedby="custIdHelp"
                           placeholder="Enter Customer Id" value={this.state.custId} onChange={this.onChangeCustId}/>
                    <small id="custIdHelp" className="form-text text-muted">Please enter your customer Id</small>
                </div>
                <div className="form-group">
                    <button type="button" className="btn btn-primary" onClick={this.savePayment}>Save</button>
                </div>
        </form>)
    }
}

const mapDispatchToProps = () => {
    return {
        addPayment
    };
};

const mapStateToProps = (state) => ({
    id:state.id,
    employees: state.paymentDate,
    type: state.type,
    amount: state.amount,
    custId: state.custId
})

export default connect(mapStateToProps,mapDispatchToProps)(AddPayment);