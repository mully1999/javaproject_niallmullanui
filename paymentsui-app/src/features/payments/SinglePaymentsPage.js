import React from 'react'
import { useSelector } from 'react-redux'

export const  SinglePaymentPage = ({ match }) => {
    const { paymentId } = match.params
    const payment = useSelector((state) =>
        state.payments.payments.find(item => item.id == paymentId)
    )

    if (!payment) {
        return (
            <section>
                <div className="jumbotron">
                    <h2>Payment not found!</h2>
                </div>
            </section>
        )
    }

    return (
        <section>
            <div className="jumbotron">
                <h1>Info for Payment ID: {payment.id}</h1>
                <br />
                <h2>Customer ID: {payment.custId}</h2>
                <h2>Transaction Type: {payment.type}</h2>
                <h2>Amount: £{payment.amount}</h2>
                <h2>Date: {payment.paymentDate}</h2>
                {/*<Link to={`/editPost/${post.id}`} className="button">*/}
                {/*    Take Me To Edit Post*/}
                {/*</Link>*/}
            </div>
        </section>
    )
}

export default SinglePaymentPage;