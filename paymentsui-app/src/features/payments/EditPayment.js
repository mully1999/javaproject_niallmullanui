import React, {useState} from 'react'
import { useSelector, useDispatch } from 'react-redux';
import {addPaymentList} from "./paymentsSlice";

export const  EditPaymentPage = ({ match }) => {

    const dispatch = useDispatch();
    const { editId } = match.params
    const editPayment = useSelector((state) =>
        state.payments.payments.find(item => item.id == editId)
    )

    console.log(editPayment)

    //editPayment constantly presents as undefined ? so this fails, I wan to
    //populate with the retrieve Payment form the store, but it
    const initialState =   {
        "id": editPayment.id,
        "paymentDate": editPayment.paymentDate,
        "type": editPayment.type,
        "amount": editPayment.amount,
        "custId": editPayment.custId
    };

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setPayment({ ...payment,  [name]: value});
    };

    const [payment, setPayment] = useState(initialState);

    return (
        <form onSubmit={(event) => {
            // maxId(paymentList);
            event.preventDefault();
            dispatch(addPaymentList(payment));
        }}>
            <h1>Edit Payment</h1>
            <div className="form-group">
                <label htmlFor="id">Payment Id</label>
                <input type="number" className="form-control" id="id"  name="id"
                       placeholder="Payment Id" value={payment.id} required onChange={handleInputChange}/>
            </div>
            <div className="form-group">
                <label htmlFor="paymentDate">Payment Date</label>
                <input type="date" className="form-control" id="paymentDate" name="paymentDate"
                       placeholder="Enter id" value={payment.paymentDate} onChange={handleInputChange}/>
            </div>
            <div className="form-group">
                <label htmlFor="paymentType">Type</label>
                <input type="text" className="form-control" id="type" name="type" aria-describedby="typeHelp"
                       placeholder="Enter Type" value={payment.type} required onChange={handleInputChange}/>
                <small id="paymentTypeHelp" className="form-text text-muted">Valid payment types are CASH, CHEQUE or CC</small>
            </div>
            <div className="form-group">
                <label htmlFor="paymentamount">Amount</label>
                <input type="number" className="form-control" id="amount" name="amount" aria-describedby="amountHelp"
                       placeholder="Enter Amount" value={payment.amount} required onChange={handleInputChange}/>
                <small id="amountHelp" className="form-text text-muted">Please enter a number greater than 0</small>
            </div>
            <div className="form-group">
                <label htmlFor="custId">Customer Id</label>
                <input type="custId" className="form-control" id="custId" name="custId" aria-describedby="custIdHelp"
                       placeholder="Enter Customer Id" value={payment.custId} required onChange={handleInputChange}/>
                <small id="custIdHelp" className="form-text text-muted">Please enter your customer Id</small>
            </div>
            <div className="form-group">
                <input type="submit" className="btn btn-primary" value="Save"/>
            </div>
        </form>
    );
}

export default EditPaymentPage;