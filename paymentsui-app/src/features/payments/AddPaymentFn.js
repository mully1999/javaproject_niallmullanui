import React, { useState }  from 'react';
import {addPaymentList} from "./paymentsSlice";
import { useSelector, useDispatch } from 'react-redux';

function ListPaymentsFn() {

    const dispatch = useDispatch();
    const paymentList = useSelector((state) => state.payments.payments)

    const initialState =   {
        //This needs to be set off the latest value in the list once we get it into redux
        "id": '',
        "paymentDate": '',
        "type": '',
        "amount": '',
        "custId": ''
    };

    const [payment, setPayment] = useState(initialState);

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setPayment({ ...payment,  [name]: value});
    };

    //Why does this not work!!!! just ends up writing 0 as Id even though I calc latest id
    const maxId = (paymentList) => {
        let res
        if (paymentList.length > 0) {
            res = Math.max.apply(Math, paymentList.map(function (payment) {
                return payment.id + 1;
            }))
            console.log('In main arm')
        } else
        {
            console.log('In the default 1 arm')
            res = 1
        }
        console.log('Setting ID as' + res)
        // console.log('This is the payment object ' + {payment.id})
        setPayment({...payment, id: res});
    }

    return (
        <form onSubmit={(event) => {
            // maxId(paymentList);
            event.preventDefault();
            dispatch(addPaymentList(payment));
        }}>
            <h1>Add Payment</h1>
            <div className="form-group">
                <label htmlFor="id">Payment Id</label>
                <input type="number" className="form-control" id="id"  name="id"
                       placeholder="Payment Id" value={payment.id} required onChange={handleInputChange}/>
            </div>
            <div className="form-group">
                <label htmlFor="paymentDate">Payment Date</label>
                <input type="date" className="form-control" id="paymentDate" name="paymentDate"
                       placeholder="Enter id" value={payment.paymentDate} onChange={handleInputChange}/>
            </div>
            <div className="form-group">
                <label htmlFor="paymentType">Type</label>
                <input type="text" className="form-control" id="type" name="type" aria-describedby="typeHelp"
                       placeholder="Enter Type" value={payment.type} required onChange={handleInputChange}/>
                <small id="paymentTypeHelp" className="form-text text-muted">Valid payment types are CASH, CHEQUE or CC</small>
            </div>
            <div className="form-group">
                <label htmlFor="paymentamount">Amount</label>
                <input type="number" className="form-control" id="amount" name="amount" aria-describedby="amountHelp"
                       placeholder="Enter Amount" value={payment.amount} required onChange={handleInputChange}/>
                <small id="amountHelp" className="form-text text-muted">Please enter a number greater than 0</small>
            </div>
            <div className="form-group">
                <label htmlFor="custId">Customer Id</label>
                <input type="custId" className="form-control" id="custId" name="custId" aria-describedby="custIdHelp"
                       placeholder="Enter Customer Id" value={payment.custId} required onChange={handleInputChange}/>
                <small id="custIdHelp" className="form-text text-muted">Please enter your customer Id</small>
            </div>
            <div className="form-group">
                <input type="submit" className="btn btn-primary" value="Save"/>
            </div>
        </form>
    );
}

export default ListPaymentsFn;
