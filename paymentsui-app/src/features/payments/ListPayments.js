import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {initPaymentList,typePaymentList} from "./paymentsSlice";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from 'react-router-dom'

export default function ListPayments() {

    const dispatch = useDispatch();
    const [byType, setType] = useState('');

    //How do I filter this based on payment Type? rather than go back to mongo running out of time so can't figure it out
    //just going to code another method to go to mongo
    const paymentList = useSelector((state) => state.payments.payments)
    const renderedPayments = paymentList.map( payment => (
            <tr key={payment.id}>
                <th scope="row">{payment.id}</th>
                <td>{payment.type}</td>
                <td>£{payment.amount}</td>
                <td><Link to={`/payments/${payment.id}`} className="button muted-button">
                    More
                </Link></td>
                <td><Link to={`/editPayment/${payment.id}`} className="button muted-button">
                    Edit
                </Link></td>
            </tr>
    ));

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setType(value);
    };

    const paymentsTable =
        // <div className="row justify-content-center">
        //     <div className="col-auto">
                <table className="table table-hover">
                    <caption>Please filter as desired</caption>
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">TYPE</th>
                        <th scope="col">AMOUNT</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    {renderedPayments}
                    </tbody>
                </table>
        //     </div>
        // </div>

    return(
        <div className="container">
            <div className="row">
                <h2>Payments</h2>
                {paymentsTable}
            </div>
            <div className="form-group">
                {/*<label htmlFor="id">Enter Type</label>*/}
                <input type="text" className="form-control" id="type"  name="type"
                       placeholder="Enter type here" value={byType} required onChange={handleInputChange}/>
            </div>
            <div className="row">
                <div className="col-sm">
                    <button type="button" className="btn btn-primary" onClick={() => dispatch(initPaymentList())}>RESET</button>
                </div>
                <div className="col-sm">
                    <button type="button" className="btn btn-primary" onClick={() => dispatch(typePaymentList(byType))}>FILTER BY TYPE</button>
                </div>
            </div>
        </div>
    );
}
