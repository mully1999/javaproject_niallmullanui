import { createSlice } from '@reduxjs/toolkit';
import PayMeService from "../../services/PaymeRestService";


const initialState =
    {
        pending: false,
        payments:[],
        error:null
}

export const paymentsSlice = createSlice({
  name: 'payments',
  initialState,
  reducers: {
    getAll: (state, action) => {
        state.payments = action.payload
    },
    addPayment: (state, action) => {
        // console.log(action.payload)
        state.payments.push(action.payload)
    }
  }
});

export const { addPayment, getAll } = paymentsSlice.actions;

export const initPaymentList = () => dispatch => {
  let payload
  PayMeService.getAll()
      .then(response => {
          payload = response.data;
          dispatch(getAll(payload));
      })
      .catch(e => {
        console.log(e);
      });
}

export const typePaymentList = (type) => dispatch => {
    let payload
    PayMeService.getByType(type)
        .then(response => {
            payload = response.data;
            dispatch(getAll(payload));
        })
        .catch(e => {
            console.log(e);
        });
}

export const addPaymentList = (data) => dispatch => {
    PayMeService.create(data)
        .then(response => {
            dispatch(addPayment(data));
        })
        .catch(e => {
            console.log(e);
        });
}

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched
// export const incrementAsync = amount => dispatch => {
//   setTimeout(() => {
//     dispatch(incrementByAmount(amount));
//   }, 1000);
// };

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
//export const selectCount = state => state.counter.value;

export default paymentsSlice.reducer;
