import React, {Component} from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import { Switch, Route, Link } from "react-router-dom"
import About from "./features/payments/About"
import AddNewPayment from "./features/payments/AddPayment"
import ListPayments from "./features/payments/ListPayments"
import SinglePaymentPage from "./features/payments/SinglePaymentsPage"
import {useDispatch} from "react-redux";
import {initPaymentList} from "./features/payments/paymentsSlice";
import AddPaymentFn from "./features/payments/AddPaymentFn"
import EditPaymentPage from "./features/payments/EditPayment"

function App() {

  // Initialise our payments list
    const dispatch = useDispatch()
    dispatch(initPaymentList())

    return (
        <div id="container">
          {/*New code for payments*/}
          <nav className="navbar navbar-expand navbar-dark bg-primary">
            <a href="/home" className="navbar-brand">
              Home
            </a>
            <div className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={"/listAll"} className="nav-link">
                  List Payments
                </Link>
              </li>
              <li className="nav-item">
                <Link to={"/add"} className="nav-link">
                  Add New Payment
                </Link>
              </li>
              <li className="nav-item">
                <Link to={"/about"} className="nav-link">
                  About
                </Link>
              </li>
            </div>
          </nav>
          <div className="container mt-3">
            <Switch>
              <Route exact path="/listAll" component={ListPayments}/>
              <Route exact path={["/", "/about"]} component={About}/>
              <Route exact path="/add" component={AddPaymentFn}/>
              <Route exact path="/payments/:paymentId" component={SinglePaymentPage} />
              <Route exact path="/editPayment/:editId" component={EditPaymentPage} />
            </Switch>
          </div>
        </div>
    );
}

export default App;
